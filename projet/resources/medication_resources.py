from flask_restful import Resource, reqparse
from projet.models.medication import MedicationModel, update_medication

# Define parser and request args
from projet.models.user import UserModel

parser = reqparse.RequestParser()
parser.add_argument('name', type=str)
parser.add_argument('strength', type=str)
parser.add_argument('strengthUnit', type=str)
parser.add_argument('dosage', type=str)
parser.add_argument('dosageUnit', type=str)
parser.add_argument('frequency', type=str)
parser.add_argument('howTaken', type=str)
parser.add_argument('reason', type=str)
parser.add_argument('comment', type=str)
parser.add_argument('idUser', type=int)


class Medication(Resource):

    def get(self, medication_idUser):
        medicationList = MedicationModel.find_by_id_user(medication_idUser)
        js = []
        if medicationList:
            for medication in medicationList:
                js.append(medication.json())
            return {'medicationList': js}
        return {'message': 'medication not found!'}, 404

    def post(self):
        args = parser.parse_args()
        if UserModel.find_by_id(args['idUser']):
            medication = MedicationModel(dosage=args['dosage'], dosageUnit=args['dosageUnit'], name=args['name'],
                                         strength=args['strength'],
                                         strengthUnit=args['strengthUnit'], frequency=args['frequency'],
                                         howTaken=args['howTaken'], comment=args['comment'], reason=args['reason'],
                                         idUser=args['idUser'])
            try:
                MedicationModel.add_medication(medication)
                return medication.json()
            except:
                return {"message": "An error occured creationg to the data base."}, 500

        return {'message': " The user account does not exist "}, 400

    def delete(self, idMedication):
        medication = MedicationModel.find_by_id_medication(idMedication)
        if medication:
            MedicationModel.delete_medication(medication)
            return {"message": "Medication has been deleted"}
        return {"message": "Medication not found!"}, 404

    def put(self):
        args = parser.parse_args()
        medication = MedicationModel.find_by_id_medication(args['idUser'])
        if medication:
            medication.name = args['name']
            medication.strength = args['strength']
            medication.strengthUnit = args['strengthUnit']
            medication.dosage = args['dosage']
            medication.dosageUnit = args['dosageUnit']
            medication.frequency = args['frequency']
            medication.howTaken = args['howTaken']
            medication.reason = args['reason']
            medication.comment = args['comment']
            update_medication()
            return {"message": "Medication has been update"}
        return {"message": "Medication not found!"}, 404
