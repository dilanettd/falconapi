

from flask import render_template, url_for, redirect
from flask_restful import Resource, reqparse

from projet import app
from projet.itsdangerous.itsdangerous import ts
from projet.models.owner import OwnerModel, update_owner
from projet.models.user import UserModel
from flask_mail import Mail, Message

# Define parser and request args
parser = reqparse.RequestParser()
parser.add_argument('lastName', type=str)
parser.add_argument('firstName', type=str)
parser.add_argument('email', type=str)
parser.add_argument('password', type=str)
parser.add_argument('confirmed', type=bool)

mail = Mail(app)


class Owner(Resource):

    def get(self, email):
        owner = OwnerModel.find_by_email(email)
        if owner:
            return owner.json()
        else:
            administrator = UserModel.find_by_administrator(email)
            if administrator:
                return administrator.json()
        return {'message': 'Owner not found!'}, 404

    def post(self):
        args = parser.parse_args()
        owner = OwnerModel(firstName=args['firstName'], lastName=args['lastName'], email=args['email'],
                           password=args['password'])
        if OwnerModel.find_by_email(owner.email):
            return {'message': " owner with a email '{}'already exists'".format(owner.email)}, 400

        try:
            OwnerModel.add_owner(owner)
            user = UserModel(firstName=owner.firstName, lastName=owner.lastName, role="Owner",
                             roleDescription="The owner of the application",
                             email=owner.email, profile="../../static/img/avatar.png", owner=owner)
            UserModel.add_user(user)
            update_owner()
            owner.currentUser = user.id
            update_owner()
            token = ts.dumps(args['email'], salt='email-confirm-key')
            confirm_url = url_for('confirm_email', token=token, _external=True)
            message = render_template("template_confirm.html", link=confirm_url)
            msg = Message('Confirm your account', recipients=[args['email']])
            msg.html = message
            mail.send(msg)
            return {"message": "success"}, 200
        except:
            return {"message": "An error occurred creating to the database."}, 500

    def delete(self, id):
        owner = OwnerModel.find_by_id(id)
        if owner:
            OwnerModel.delete_owner(owner)
            return {"message": "Owner has been removed"}
        return {"message": "Owner not found!"}, 404

    def put(self):
        args = parser.parse_args()
        email = args['email']
        password = args['password']
        owner = OwnerModel.find_by_email(email)
        if owner and not password:
            token = ts.dumps(email, salt='email-confirm-key')
            confirm_url = url_for('reset_password', token=token, _external=True)
            message = render_template("template_reset_password.html", link=confirm_url)
            msg = Message('Reset Password', recipients=[email])
            msg.html = message
            mail.send(msg)
            return {"message": "success"}, 200
        if owner and password:
            owner.password = password
            update_owner()
            return {"message": "success"}, 200
        return {"message": "Owner not found!"}, 404


@app.route('/confirm/<token>')
def confirm_email(token):
    try:
        email = ts.loads(token, salt="email-confirm-key", max_age=86400)
        owner = OwnerModel.find_by_email(email)
        owner.confirmed = True
        update_owner()
        return redirect('http://localhost:8000/login')
    except:
        return {"message": "Something went wrong!"}, 500


@app.route('/reset/<token>')
def reset_password(token):
    try:
        email = ts.loads(token, salt="email-confirm-key", max_age=3600)
        owner = OwnerModel.find_by_email(email)
        if owner:
            return redirect('http://localhost:8000/reset/' + email)
    except:
        return {"message": "Something went wrong!"}, 500
