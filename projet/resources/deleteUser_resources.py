from flask_restful import Resource, reqparse

# Define parser and request args
from projet.models.deleteUser import DeleteUserModel
import datetime

from projet.models.user import UserModel


class DeleteUser(Resource):

    def get(self, user_identifier):
        userList = DeleteUserModel.find_by_id_owner(user_identifier)
        js = []
        if userList:
            for ob in userList:
                js.append(ob.json())
            print(js)
            return {'deleteList': js}
        return {'message': 'No user deletions found!'}, 404

    def delete(self, user_identifier):
        deleteUser = DeleteUserModel.find_by_id(user_identifier)
        user = UserModel.find_by_id(deleteUser.idUser)
        if user:
            UserModel.delete_user(user)
            return {"message": "The user has been removed"}
        return {"message": "User not found!"}, 404

    def put(self, user_identifier):
        deleteUser = DeleteUserModel.find_by_id(user_identifier)
        if deleteUser:
            user = UserModel.find_by_id(deleteUser.idUser)
            if user:
                user.deleteOrNot = False
            DeleteUserModel.delete_delete_user(deleteUser)
            return {"message": "success"}
        return {"message": "Measurement not found!"}, 404


def delete_user_expire():
    userList = DeleteUserModel.find_by_datetime(datetime.datetime.now().strftime('%Y-%m-%d %H:%M'))
    for user in userList:
        delete = UserModel.find_by_id(user.idUser)
        if delete:
            UserModel.delete_user(delete)
