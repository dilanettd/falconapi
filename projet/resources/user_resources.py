import datetime

from flask_restful import Resource, reqparse

from projet.models.deleteUser import DeleteUserModel
from projet.models.owner import OwnerModel
from projet.models.user import UserModel, update_user

# Define parser and request args
parser = reqparse.RequestParser()
parser.add_argument('firstName', type=str)
parser.add_argument('lastName', type=str)
parser.add_argument('middleName', type=str)
parser.add_argument('email', type=str)
parser.add_argument('password', type=str)
parser.add_argument('role', type=str)
parser.add_argument('title', type=str)
parser.add_argument('suffix', type=str)
parser.add_argument('otherPatientName', type=str)
parser.add_argument('uniqueIdentifier', type=str)
parser.add_argument('dateOfBirth', type=str)
parser.add_argument('age', type=str)
parser.add_argument('sex', type=str)
parser.add_argument('height', type=str)
parser.add_argument('heightUnit', type=str)
parser.add_argument('weight', type=str)
parser.add_argument('weightUnit', type=str)
parser.add_argument('country', type=str)
parser.add_argument('location', type=str)
parser.add_argument('contact', type=str)
parser.add_argument('address', type=str)
parser.add_argument('addressInfo', type=str)
parser.add_argument('institutionName', type=str)
parser.add_argument('department', type=str)
parser.add_argument('referringPhysician', type=str)
parser.add_argument('requestingPhysician', type=str)
parser.add_argument('admissionID', type=str)
parser.add_argument('performingPhysician', type=str)
parser.add_argument('profile', type=str)
parser.add_argument('idOwner', type=int)
parser.add_argument('current', type=str)
parser.add_argument('idUser', type=str)


class User(Resource):

    def get(self, idOwner):
        userList = UserModel.find_by_id_owner(idOwner)
        js = []
        if userList:
            for ob in userList:
                js.append(ob.json())
            return {'userList': js}
        return {'message': 'user not found!'}, 404

    def post(self):
        args = parser.parse_args()
        user = UserModel(firstName=args['firstName'], lastName=args['lastName'], middleName=args['middleName'],
                         email=args['email'], title=args['title'],
                         suffix=args['suffix'], otherPatientName=args['otherPatientName'],
                         uniqueIdentifier=args['uniqueIdentifier'], dateOfBirth=args['dateOfBirth'], sex=args['sex'],
                         height=args['height'], heightUnit=args['heightUnit'], weight=args['weight'],
                         weightUnit=args['weightUnit'], country=args['country'], location=args['location'],
                         contact=args['contact'], address=args['address'], addressInfo=args['addressInfo'],
                         institutionName=args['institutionName'], department=args['department'],
                         referringPhysician=args['referringPhysician'], requestingPhysician=args['requestingPhysician'],
                         admissionID=args['admissionID'], performingPhysician=args['performingPhysician'],
                         profile=args['profile'], idOwner=args['idOwner'])
        if UserModel.find_by_email(user.email):
            return {'message': " user with a email '{}'already exists'".format(user.email)}, 400
        try:
            UserModel.add_user(user)
            return {"message": "success"}, 200
        except:
            return {"message": "An error occurred creating to the database."}, 500

    def delete(self, identifier):
        user = UserModel.find_by_id(identifier)
        if user:
            delete_user = DeleteUserModel(firstName=user.firstName, lastName=user.lastName,
                                          profile=user.profile, uniqueIdentifier=user.uniqueIdentifier,
                                          deleteDate=datetime.datetime.now().strftime('%Y-%m-%d %H:%M'),
                                          permanenlyDeleteDate=calculate_deleting_date(),
                                          idUser=identifier, idOwner=user.idOwner)
            DeleteUserModel.add_delete_user(delete_user)
            user.deleteOrNot = True
            update_user()
            return {"message": "success"}
        return {"message": "User not found!"}, 404

    def put(self):
        args = parser.parse_args()
        idUser = args['idUser']
        user = UserModel.find_by_id(idUser)
        if user:
            user.firstName = args['firstName']
            user.lastName = args['lastName']
            user.middleName = args['middleName']
            user.email = args['email']
            user.password = args['password']
            user.title = args['title']
            user.suffix = args['suffix']
            user.otherPatientName = args['otherPatientName']
            user.uniqueIdentifier = args['uniqueIdentifier']
            user.dateOfBirth = args['dateOfBirth']
            user.sex = args['sex']
            user.height = args['height']
            user.heightUnit = args['heightUnit']
            user.weight = args['weight']
            user.weightUnit = args['weightUnit']
            user.country = args['country']
            user.location = args['location']
            user.contact = args['contact']
            user.address = args['address']
            user.addressInfo = args['addressInfo']
            user.institutionName = args['institutionName']
            user.department = args['department']
            user.referringPhysician = args['referringPhysician']
            user.requestingPhysician = args['requestingPhysician']
            user.admissionID = args['admissionID']
            user.performingPhysician = args['performingPhysician']
            user.profile = args['profile']
            update_user()
            return {"message": "notes has been updated"}
        return {"message": "Measurement not found!"}, 404


class DeleteUser(Resource):
    def get(self, idOwner):
        userList = UserModel.find_by_id_owner(idOwner)
        js = []
        if userList:
            for ob in userList:
                js.append(ob.json())
            return {'userList': js}
        return {'message': 'user not found!'}, 404


class Role(Resource):
    def post(self):
        args = parser.parse_args()
        print(args['role'])
        password = args['password']
        role = args['role']
        current = args['current']
        idUser = args['idUser']

        if role == "User":
            roleDescription = "Whoever has the measurements"
        else:
            roleDescription = "It assumes the rights of the owner and creates user"
        try:
            user = UserModel.find_by_id(idUser)
            if (user):
                user.password = password
                user.role = role
                user.roleDescription = roleDescription
                update_user()
                if current == "on":
                    Owner = OwnerModel.find_by_id(user.idOwner)
                    if (Owner):
                        Owner.currentUser = idUser
                        update_user()
            return {"message": "success"}, 200
        except:
            return {"message": "An error occurred creating to the database."}, 500


class Users(Resource):

    def get(self, idUser):
        user = UserModel.find_by_id(idUser)
        if user:
            return user.json()
        return {'message': 'Owner not found!'}, 404


def calculate_deleting_date():
    date = datetime.datetime.now().strftime('%Y-%m-%d')
    date = datetime.datetime.strptime(date, '%Y-%m-%d')
    return date + datetime.timedelta(days=30)
