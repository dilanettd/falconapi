from flask_restful import Resource, reqparse

# Define parser and request args
from projet.models.setting import SettingModel

parser = reqparse.RequestParser()
parser.add_argument('language', type=str)
parser.add_argument('theme', type=str)


class Setting(Resource):

    def get(self, identifier):
        setting = SettingModel.find_by_id_owner(identifier)
        if setting:
            return setting.json()
        return {'message': 'setting not found!'}, 404

    def post(self):
        args = parser.parse_args()
        setting = SettingModel(language=args['language'], theme=args['theme'])
        try:
            SettingModel.add_setting(setting)
        except:
            return {"message": "An error occured saving the setting"}, 500

    def delete(self, identifier):
        setting = SettingModel.find_by_id_owner(identifier)
        if setting:
            SettingModel.delete_setting(setting)
            return {"message": "The setting has been removed"}
        return {"message": "The setting not found!"}, 404
