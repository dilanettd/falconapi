from flask import render_template
from flask_restful import Resource, reqparse

from projet import app
from projet.models.measurement import MeasurementModel, update_measurement
# Email
from flask_mail import Mail, Message

# Define parser and request args
from projet.models.user import UserModel

parser = reqparse.RequestParser()
parser.add_argument('systolic', type=str)
parser.add_argument('diastolic', type=str)
parser.add_argument('pulse', type=str)
parser.add_argument('date', type=str)
parser.add_argument('time', type=str)
parser.add_argument('bodyPosition', type=str)
parser.add_argument('armLocation', type=str)
parser.add_argument('medications', type=str)
parser.add_argument('notes', type=str)
parser.add_argument('idUser', type=int)


mail = Mail(app)


class Measurement(Resource):

    def get(self, id):
        measurement = MeasurementModel.find_by_id_user(id)
        js = []
        if measurement:
            for ob in measurement:
                js.append(ob.json())
            return {'measurement': js}
        return {'message': 'measurement not found!'}, 404

    def post(self):
        args = parser.parse_args()
        user = UserModel.find_by_id(args['idUser'])
        if user:
            measurement = MeasurementModel(date=args['date'], time=args['time'], systolic=args['systolic'],
                                           diastolic=args['diastolic'],
                                           pulse=args['pulse'], bodyPosition=args['bodyPosition'],
                                           armLocation=args['armLocation'], medications=args['medications'],
                                           notes=args['notes'], idUser=args['idUser'])

            MeasurementModel.add_measurement(measurement)
            alert(args['systolic'], args['diastolic'], user.email)
            #     return measurement.json()
            # except:
            #     return {"message": "An error occured creationg to the data base."}, 500

        return {'message': " The user account does not exist "}, 400

    def delete(self, id):
        measurement = MeasurementModel.find_by_id_measurement(id)
        if measurement:
            MeasurementModel.delete_measurement(measurement)
            return {"message": "Measurement has been deleted"}
        return {"message": "Measurement not found!"}, 404

    def put(self, id):
        args = parser.parse_args()
        notes = args['notes']
        measurement = MeasurementModel.find_by_id_measurement(id)
        if measurement:
            measurement.notes = notes
            update_measurement()
            return {"message": "notes has been updated"}
        return {"message": "Measurement not found!"}, 404


def alert(sys, dia, email):
    if 140 <= int(sys) <= 159:
        if 90 <= int(dia) <= 99:
            infos = "Have your doctor check your blood pressure regularly"
            message = render_template("template_email.html", message=infos, sys=sys, dia=dia)
            msg = Message('Alert after your last blood pressure measurement', recipients=[email])
            msg.html = message
            mail.send(msg)
    elif 160 <= int(sys) <= 179:
        if 100 <= int(dia) <= 109:
            infos = "Seek medical advice as you have moderate hypertension"
            message = render_template("template_email.html", message=infos, sys=sys, dia=dia)
            msg = Message('Alert after your last blood pressure measurement', recipients=[email])
            msg.html = message
            mail.send(msg)
    elif int(sys) >= 180:
        if int(dia) >= 110:
            infos = "Consult your doctor quickly as you appear to have severe hypertension"
            message = render_template("template_email.html", message=infos, sys=sys, dia=dia)
            msg = Message('Alert after your last blood pressure measurement', recipients=[email])
            msg.html = message
            mail.send(msg)
