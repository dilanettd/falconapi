from itsdangerous import URLSafeTimedSerializer

from config import SECRET_KEY

ts = URLSafeTimedSerializer(SECRET_KEY, salt='email-confirm-key')