
from projet.models import db
from projet.models.oauth2 import Client


def populateOauth2():
    clientWeb = Client(
        name='clientWeb', client_id='clientWeb', client_secret='clientWeb#15§', client_type='web',
        _redirect_uris=(
            'http://localhost:8000/authorized '
            'http://localhost/authorized'
        ),
    )
    clientAndroid = Client(
        name='clientAndroid', client_id='clientAndroid', client_secret='clientAndroid#15§', client_type='android',
        _redirect_uris=(
            'http://localhost:8000/authorized '
            'http://localhost/authorized'
        ),
    )
    clientIOS = Client(
        name='clientIOS', client_id='clientIOS', client_secret='clientIOS#15§', client_type='ios',
        _redirect_uris=(
            'http://localhost:8000/authorized '
            'http://localhost/authorized'
        ),
    )
    db.session.add(clientWeb)
    db.session.add(clientAndroid)
    db.session.add(clientIOS)
    db.session.commit()
