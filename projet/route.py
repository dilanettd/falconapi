import schedule as schedule
from apscheduler.schedulers.blocking import BlockingScheduler
from flask import url_for, g, request, make_response, jsonify
from werkzeug.utils import redirect

from projet import app
from flask_restful import Api

from projet.itsdangerous.itsdangerous import ts
from projet.models import db
from projet.models.oauth2 import default_provider, Client
from projet.models.owner import OwnerModel
from projet.models.user import UserModel
from projet.populate import generateUsers, generateMeasurement
from projet.populate_oauth2 import populateOauth2

from projet.resources.deleteUser_resources import DeleteUser, delete_user_expire
from projet.resources.medication_resources import Medication
from projet.resources.setting_resources import Setting
from projet.resources.user_resources import User, Role, Users
from projet.resources.measurement_resources import Measurement
from projet.resources.owner_resources import Owner
import tzlocal

oauth = default_provider(app)
api = Api(app, decorators=[oauth.require_oauth()])
db.create_all()


# sched = BlockingScheduler(timezone="Europe/Berlin")
#
#
# @sched.scheduled_job('cron', hour=16)
# def timed_job():
#     delete_user_expire()
#
#
# sched.start()

# Server oauth2


@app.route('/oauth/authorize', methods=['GET', 'POST'])
@oauth.authorize_handler
def authorize(*args, **kwargs):
    confirm = 'no'
    # NOTICE: for real project, you need to require login
    if request.method == 'POST':
        print("Post")
    if request.method == 'GET':
        # render a page for user to confirm the authorization
        client_id = kwargs.get('client_id')
        client = Client.query.filter_by(client_id=client_id).first()
        if client:
            confirm = 'yes'

    if request.method == 'HEAD':
        # if HEAD is supported properly, request parameters like
        # client_id should be validated the same way as for 'GET'
        response = make_response('', 200)
        response.headers['X-Client-ID'] = kwargs.get('client_id')
        return response

    return confirm == 'yes'


@app.route('/oauth/token', methods=['POST', 'GET'])
@oauth.token_handler
def access_token():
    return {}


@app.route('/oauth/revoke', methods=['POST'])
@oauth.revoke_handler
def revoke_token():
    pass


@oauth.invalid_response
def require_oauth_invalid(req):
    return jsonify(message=req.error_message), 401


api.add_resource(Owner, '/api/owner/')
api.add_resource(Owner, '/api/owner/<string:email>', endpoint='/<string:email>')
api.add_resource(User, '/api/users/get/<string:idOwner>', endpoint='/<string:idOwner>')
api.add_resource(Users, '/api/user/get/<string:idUser>', endpoint='/<string:idUser>')
api.add_resource(User, '/api/user/add/')
api.add_resource(User, '/api/user/delete/<string:identifier>', endpoint='/<string:identifier>')
api.add_resource(Measurement, '/api/measurement/<string:id>', endpoint='/<string:id>')
api.add_resource(Measurement, '/api/measurement/')
api.add_resource(Setting, '/api/setting/<int:id>', endpoint='/<int:idfdsd>')
api.add_resource(Setting, '/api/setting/')
api.add_resource(Role, '/api/role/')
api.add_resource(DeleteUser, '/api/delete_user/<string:user_identifier>')
api.add_resource(Medication, '/api/medication/<string:medication_idUser>', endpoint='/<string:medication_idUser>')
api.add_resource(Medication, '/api/medication/delete/<string:idMedication>', endpoint='/<string:idMedication>')
api.add_resource(Medication, '/api/medication/')

populateOauth2()
# generateMeasurement()
# generateUsers(500)
