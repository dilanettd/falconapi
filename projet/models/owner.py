from sqlalchemy.orm import backref
from projet.models import db


def update_owner():
    db.session.commit()


class OwnerModel(db.Model):
    __tablename__ = 'owner'
    id = db.Column(db.Integer, primary_key=True)
    firstName = db.Column(db.String(256))
    lastName = db.Column(db.String(256))
    email = db.Column(db.String(256))
    password = db.Column(db.String(256))
    confirmed = db.Column(db.Boolean, nullable=False, default=False)
    currentUser = db.Column(db.String(256))
    user = db.relationship("UserModel", cascade="all, delete", backref=backref("owner", passive_deletes=True))
    setting = db.relationship("SettingModel", cascade="all, delete", backref=backref("owner", passive_deletes=True))
    deleteuser = db.relationship("DeleteUserModel", cascade="all, delete", backref=backref("owner", passive_deletes=True))

    def json(self):
        return {'id': self.id,'firstName': self.firstName, 'lastName': self.lastName, 'email': self.email,
                'password': self.password, 'currentUser': self.currentUser, 'confirmed': self.confirmed}

    @classmethod
    def find_by_email(cls, email):
        return cls.query.filter_by(email=email).first()

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    def add_owner(self):
        db.session.add(self)
        db.session.commit()

    def delete_owner(self):
        db.session.delete(self)
        db.session.commit()
