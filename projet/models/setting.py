from projet.models import db


class SettingModel(db.Model):
    __tablename__ = 'setting'
    id = db.Column(db.Integer, primary_key=True)
    language = db.Column(db.String(256))
    theme = db.Column(db.String(256))
    idOwner = db.Column(db.Integer, db.ForeignKey('owner.id', ondelete='CASCADE'))

    def json(self):
        return {'id': self.id, 'language': self.language, 'theme': self.theme,
                'idOwner': self.idOwner}

    @classmethod
    def find_by_id_owner(cls, id):
        return cls.query.filter_by(idOwner=id).first()

    def add_setting(self):
        db.session.add(self)
        db.session.commit()

    def delete_setting(self):
        db.session.delete(self)
        db.session.commit()
