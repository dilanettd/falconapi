from sqlalchemy.orm import backref
from projet.models import db


def update_user():
    db.session.commit()


class UserModel(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    firstName = db.Column(db.String(256))
    lastName = db.Column(db.String(256))
    middleName = db.Column(db.String(256))
    email = db.Column(db.String(256))
    password = db.Column(db.String(256))
    role = db.Column(db.String(256), nullable=False, default="User")
    roleDescription = db.Column(db.String(256), nullable=False, default="Whoever has the measurements")
    title = db.Column(db.String(256))
    suffix = db.Column(db.String(256))
    otherPatientName = db.Column(db.String(256))
    uniqueIdentifier = db.Column(db.String(256))
    dateOfBirth = db.Column(db.String(256))
    sex = db.Column(db.String(256))
    height = db.Column(db.String(256))
    heightUnit = db.Column(db.String(256))
    weight = db.Column(db.String(256))
    weightUnit = db.Column(db.String(256))
    country = db.Column(db.String(256))
    location = db.Column(db.String(256))
    contact = db.Column(db.String(256))
    address = db.Column(db.String(256))
    addressInfo = db.Column(db.String(256))
    institutionName = db.Column(db.String(256))
    department = db.Column(db.String(256))
    referringPhysician = db.Column(db.String(256))
    requestingPhysician = db.Column(db.String(256))
    admissionID = db.Column(db.String(256))
    performingPhysician = db.Column(db.String(256))
    deleteOrNot = db.Column(db.Boolean, default=False)
    profile = db.Column(db.String(256))
    idOwner = db.Column(db.Integer, db.ForeignKey('owner.id', ondelete='CASCADE'))
    measurement = db.relationship("MeasurementModel", cascade="all, delete",
                                  backref=backref('user', passive_deletes=True))
    medication = db.relationship("MedicationModel", cascade="all, delete",
                                 backref=backref('user', passive_deletes=True))
    deleteuser = db.relationship("DeleteUserModel", cascade="all, delete",
                                 backref=backref('user', passive_deletes=True))

    def json(self):
        return {'id': self.id, 'firstName': self.firstName, 'lastName': self.lastName, 'middleName': self.middleName,
                'email': self.email, 'password': self.password, 'role': self.role,
                'roleDescription': self.roleDescription, 'title': self.title,
                'suffix': self.suffix, 'otherPatientName': self.otherPatientName, 'uniqueIdentifier':
                    self.uniqueIdentifier, 'dateOfBirth': self.dateOfBirth, 'sex': self.sex,
                'height': self.height, 'heightUnit': self.heightUnit, 'weight': self.weight,
                'weightUnit': self.weightUnit, 'country': self.country, 'location': self.location,
                'contact': self.contact, 'address': self.address, 'addressInfo': self.addressInfo, 'institutionName':
                    self.institutionName, 'department': self.department,
                'referringPhysician': self.referringPhysician,
                'admissionID': self.admissionID, 'performingPhysician': self.performingPhysician, 'deleteOrNot':
                    self.deleteOrNot, 'profile': self.profile, 'idOwner': self.idOwner}

    @classmethod
    def find_by_id_owner(cls, id):
        return cls.query.filter_by(idOwner=id, deleteOrNot=False).all()

    def find_current_user(cls, email):
        return cls.query.filter_by(email=email).first()

    @classmethod
    def find_by_email(cls, email):
        return cls.query.filter_by(email=email, role="Administrator").first()

    @classmethod
    def find_by_administrator(cls, email):
        return cls.query.filter_by(email=email, role="Administrator").first()

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    def add_user(self):
        db.session.add(self)
        db.session.commit()

    def delete_user(self):
        db.session.delete(self)
        db.session.commit()
