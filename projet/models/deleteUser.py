from projet.models import db


class DeleteUserModel(db.Model):
    __tablename__ = 'deleteuser'
    id = db.Column(db.Integer, primary_key=True)
    firstName = db.Column(db.String(256))
    lastName = db.Column(db.String(256))
    profile = db.Column(db.String(256))
    uniqueIdentifier = db.Column(db.String(256))
    deleteDate = db.Column(db.String(256))
    permanenlyDeleteDate = db.Column(db.String(256))
    idUser = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))
    idOwner = db.Column(db.Integer, db.ForeignKey('owner.id', ondelete='CASCADE'))

    def json(self):
        return {'id': self.id, 'firstName': self.firstName, 'lastName': self.lastName, 'profile': self.profile,
                'uniqueIdentifier': self.uniqueIdentifier, 'deleteDate': self.deleteDate,
                'permanentlyDeleteDate': self.permanenlyDeleteDate, 'idUser': self.idUser, 'idOwner': self.idOwner}

    @classmethod
    def find_by_id_owner(cls, id):
        return cls.query.filter_by(idOwner=id).all()

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    def add_delete_user(self):
        db.session.add(self)
        db.session.commit()

    def delete_delete_user(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def find_by_datetime(cls, datetime):
        return cls.query(DeleteUserModel).filter(DeleteUserModel.permanenlyDeleteDate <= datetime).all()


