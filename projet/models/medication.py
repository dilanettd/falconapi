from projet.models import db


def update_medication():
    db.session.commit()


class MedicationModel(db.Model):
    __tablename__ = 'medication'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256))
    strength = db.Column(db.String(256))
    strengthUnit = db.Column(db.String(256))
    dosage = db.Column(db.String(256))
    dosageUnit = db.Column(db.String(256))
    frequency = db.Column(db.String(256))
    howTaken = db.Column(db.String(256))
    reason = db.Column(db.String(256))
    comment = db.Column(db.String(256))
    idUser = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))

    def json(self):
        return {'id': self.id, 'name': self.name, 'strength': self.strength, 'strengthUnit': self.strengthUnit,
                'dosage': self.dosage, 'dosageUnit': self.dosageUnit, 'frequency': self.frequency, 'howTaken': self.howTaken,
                'reason': self.reason, 'comment': self.comment, 'idUser': self.idUser}

    @classmethod
    def find_by_id_user(cls, id):
        return cls.query.filter_by(idUser=id).all()

    @classmethod
    def find_by_id_medication(cls, id):
        return cls.query.filter_by(id=id).first()

    def add_medication(self):
        db.session.add(self)
        db.session.commit()

    def delete_medication(self):
        db.session.delete(self)
        db.session.commit()
