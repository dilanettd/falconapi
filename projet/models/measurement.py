from projet.models import db


def update_measurement():
    db.session.commit()


class MeasurementModel(db.Model):
    __tablename__ = 'measurement'
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.String(256))
    time = db.Column(db.String(256))
    systolic = db.Column(db.String(256))
    diastolic = db.Column(db.String(256))
    pulse = db.Column(db.String(256))
    bodyPosition = db.Column(db.String(256))
    armLocation = db.Column(db.String(256))
    medications = db.Column(db.String(256))
    notes = db.Column(db.String(256))
    idUser = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))

    def json(self):
        return {'id': self.id, 'date': self.date, 'time': self.time, 'systolic': self.systolic,
                'diastolic': self.diastolic,
                'pulse': self.pulse, 'bodyPosition': self.bodyPosition, 'medications': self.medications, 'armLocation': self.armLocation,
                'notes': self.notes, 'idUser': self.idUser}

    @classmethod
    def find_by_id_user(cls, id):
        return cls.query.filter_by(idUser=id).all()

    @classmethod
    def find_by_id_measurement(cls, id):
        return cls.query.filter_by(id=id).first()

    def add_measurement(self):
        db.session.add(self)
        db.session.commit()

    def delete_measurement(self):
        db.session.delete(self)
        db.session.commit()
