import random
import string
from datetime import datetime
from datetime import timedelta
from random import randrange

from faker import Faker

from projet.models import db
from projet.models.measurement import MeasurementModel
from projet.models.owner import OwnerModel, update_owner
from projet.models.user import UserModel

# initialize a generator
faker = Faker()


def random_date(start, end):
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)


def generateMeasurement():
    owner = OwnerModel(firstName=faker.first_name(), lastName=faker.last_name(), email=faker.email(),
                       password=faker.password(), confirmed=True)
    user = UserModel(firstName=owner.firstName, lastName=owner.lastName, email=owner.email,
                     profile="../../static/img/avatar.png", owner=owner)
    db.session.add(owner)
    db.session.add(user)
    db.session.commit()
    owner.currentUser = user.id
    update_owner()
    bodyPositionList = ["Sitting", "Standing", "Not set", "Lying down"]
    armLocationList = ["Left wrist", "Not set", "Right wrist", "Left upper arm", "Right upper arm"]
    datetimeList = []
    for t in range(0, 3000):
        d1 = datetime.strptime('1/1/2019 10:30', '%m/%d/%Y %I:%M')
        d2 = datetime.strptime('1/16/2022 11:00', '%m/%d/%Y %I:%M')
        datetimeList.append(str(random_date(d1, d2)))
        datetimeList.sort()
    for i in range(0, 3000):
        date = datetimeList[i].split()[0]
        time = (datetimeList[i].split()[1])[0:5]
        sys = random.randint(90, 190)
        if sys < 120:
            dia = random.randint(60, 79)
            notes = "Optimal"
        if 120 <= sys <= 129:
            dia = random.randint(80, 84)
            notes = "Normal"
        elif 130 <= sys <= 139:
            dia = random.randint(85, 89)
            notes = "High Normal"
        elif 140 <= sys <= 159:
            dia = random.randint(90, 99)
            notes = " Mild Hypertension"
        elif 160 <= sys <= 179:
            dia = random.randint(100, 109)
            notes = " Moderate Hypertension"
        else:
            dia = random.randint(110, 119)
            notes = " Severe Hypertension"
        pulse = random.randint(60, 100)
        bodyPosition = random.choice(bodyPositionList)
        armLocation = random.choice(armLocationList)
        measurement = MeasurementModel(date=date, time=time, systolic=sys, diastolic=dia, pulse=pulse,
                                       bodyPosition=bodyPosition, armLocation=armLocation,
                                       notes=notes, user=user)
        db.session.add(measurement)
        db.session.commit()


def generateUsers(size):
    for i in range(0, size):
        firstName = faker.first_name()
        lastName = faker.last_name()
        uniqueIdentifier = "USR" + ''.join([random.choice(string.ascii_uppercase
                                                          + string.digits) for n in range(4)])
        contact = faker.phone_number()
        email = faker.email()
        dateOfBirth = faker.date_of_birth()
        idOwner = 2
        user = UserModel(firstName=firstName, lastName=lastName, uniqueIdentifier=uniqueIdentifier, contact=contact,
                         email=email, dateOfBirth=dateOfBirth, idOwner=idOwner)
        db.session.add(user)
        db.session.commit()
